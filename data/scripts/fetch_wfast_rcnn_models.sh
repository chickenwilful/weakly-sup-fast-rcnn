#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $DIR

FILE=wfast_rcnn_models.tgz
URL=www.dropbox.com/s/l4kyjmqvex4ipr5/wfast_rcnn_models.tgz?dl=1
CHECKSUM=4a66bdc0091757d817b27415df256a59

if [ -f $FILE ]; then
  echo "File already exists. Checking md5..."
  os=`uname -s`
  if [ "$os" = "Linux" ]; then
    checksum=`md5sum $FILE | awk '{ print $1 }'`
  elif [ "$os" = "Darwin" ]; then
    checksum=`cat $FILE | md5`
  fi
  echo "Checksum: $checksum"

  if [ "$checksum" = "$CHECKSUM" ] ; then
    echo "Checksum is correct. No need to download."
    exit 0
  else
    echo "Checksum is incorrect. Need to download again."
  fi
fi

echo "Downloading W-FRCN demo models (0.96G)..."

wget $URL -O $FILE

echo "Unzipping..."

tar zxvf $FILE

echo "Done. Please run this command again to verify that checksum = $CHECKSUM."
