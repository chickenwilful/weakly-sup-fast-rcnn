# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------
# Modified by Hoang Yen for W-FRCN

from wfast_rcnn.config import cfg, get_output_dir
from utils.timer import Timer
import numpy as np
import cv2
from utils.cython_nms import nms
from utils.heatmap import get_heatmap_dets, get_heatmap, draw_heatmap
from utils.localisation import get_locs, draw_localisations
import cPickle
import heapq
from utils.blob import im_list_to_blob
import os


def _get_image_blob(im):
    """Converts an image into a network input.

    Arguments:
        im (ndarray): a color image in BGR order

    Returns:
        blob (ndarray): a data blob holding an image pyramid
        im_scale_factors (list): list of image scales (relative to im) used
            in the image pyramid
    """
    im_orig = im.astype(np.float32, copy=True)
    im_orig -= cfg.PIXEL_MEANS

    im_shape = im_orig.shape
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])

    processed_ims = []
    im_scale_factors = []

    for target_size in cfg.TEST.SCALES:
        im_scale = float(target_size) / float(im_size_min)
        # Prevent the biggest axis from being more than MAX_SIZE
        if np.round(im_scale * im_size_max) > cfg.TEST.MAX_SIZE:
            im_scale = float(cfg.TEST.MAX_SIZE) / float(im_size_max)
        im = cv2.resize(im_orig, None, None, fx=im_scale, fy=im_scale,
                        interpolation=cv2.INTER_LINEAR)
        im_scale_factors.append(im_scale)
        processed_ims.append(im)

    # Create a blob to hold the input images
    blob = im_list_to_blob(processed_ims)

    return blob, np.array(im_scale_factors)


def _get_rois_blob(im_rois, im_scale_factors):
    """Converts RoIs into network inputs.

    Arguments:
        im_rois (ndarray): R x 4 matrix of RoIs in original image coordinates
        im_scale_factors (list): scale factors as returned by _get_image_blob

    Returns:
        blob (ndarray): R x 5 matrix of RoIs in the image pyramid
    """
    rois, levels = _project_im_rois(im_rois, im_scale_factors)
    rois_blob = np.hstack((levels, rois))
    return rois_blob.astype(np.float32, copy=False)


def _project_im_rois(im_rois, scales):
    """Project image RoIs into the image pyramid built by _get_image_blob.

    Arguments:
        im_rois (ndarray): R x 4 matrix of RoIs in original image coordinates
        scales (list): scale factors as returned by _get_image_blob

    Returns:
        rois (ndarray): R x 4 matrix of projected RoI coordinates
        levels (list): image pyramid levels used by each projected RoI
    """
    im_rois = im_rois.astype(np.float, copy=False)

    if len(scales) > 1:
        widths = im_rois[:, 2] - im_rois[:, 0] + 1
        heights = im_rois[:, 3] - im_rois[:, 1] + 1

        areas = widths * heights
        scaled_areas = areas[:, np.newaxis] * (scales[np.newaxis, :] ** 2)
        diff_areas = np.abs(scaled_areas - 224 * 224)
        levels = diff_areas.argmin(axis=1)[:, np.newaxis]
    else:
        levels = np.zeros((im_rois.shape[0], 1), dtype=np.int)

    rois = im_rois * scales[levels]

    return rois, levels


def _get_blobs(im, rois):
    """Convert an image and RoIs within that image into network inputs."""
    blobs = {'data': None, 'rois': None}
    blobs['data'], im_scale_factors = _get_image_blob(im)
    blobs['rois'] = _get_rois_blob(rois, im_scale_factors)
    return blobs, im_scale_factors


def im_detect(net, im, boxes):
    """Detect object classes in an image given object proposals.

    Arguments:
        net (caffe.Net): Fast R-CNN network to use
        im (ndarray): color image to test (in BGR order)
        boxes (ndarray): R x 4 array of object proposals

    Returns:
        im_scores (ndarray): 1 X K array of image class scores
        scores (ndarray): R x K array of object class scores
        boxes (ndarray): R x (4*K) array of predicted bounding boxes
    """
    blobs, unused_im_scale_factors = _get_blobs(im, boxes)

    # When mapping from image ROIs to feature map ROIs, there's some aliasing
    # (some distinct image ROIs get mapped to the same feature ROI).
    # Here, we identify duplicate feature ROIs, so we only compute features
    # on the unique subset.
    if cfg.DEDUP_BOXES > 0:
        v = np.array([1, 1e3, 1e6, 1e9, 1e12])
        hashes = np.round(blobs['rois'] * cfg.DEDUP_BOXES).dot(v)
        _, index, inv_index = np.unique(hashes, return_index=True,
                                        return_inverse=True)
        blobs['rois'] = blobs['rois'][index, :]
        boxes = boxes[index, :]

    # reshape network inputs
    net.blobs['data'].reshape(*(blobs['data'].shape))
    net.blobs['rois'].reshape(*(blobs['rois'].shape))
    blobs_out = net.forward(data=blobs['data'].astype(np.float32, copy=False),
                            rois=blobs['rois'].astype(np.float32, copy=False))

    roi_scores = net.blobs['roi_score'].data
    roi_scores = roi_scores.reshape((roi_scores.shape[0], roi_scores.shape[1]))
    im_scores = blobs_out['im_score']
    im_scores = im_scores.reshape((im_scores.shape[2]))

    # Simply repeat the boxes, once for each class
    pred_boxes = np.tile(boxes, (1, roi_scores.shape[1]))

    if cfg.DEDUP_BOXES > 0:
        # Map scores and predictions back to the original set of boxes
        roi_scores = roi_scores[inv_index, :]
        pred_boxes = pred_boxes[inv_index, :]

    return im_scores, roi_scores, pred_boxes


def vis_localisation(imdb, im_ind, class_ind, im_score, dets):
    """Visual debugging of localisation."""

    if dets.shape[0] == 0:
        return

    gt_boxes = imdb.gt_roidb()[im_ind]['boxes'] \
        [np.where(imdb.gt_roidb()[im_ind]['gt_classes'] == class_ind)]
    im_index = imdb.image_index[im_ind]
    class_name = imdb.classes[class_ind]

    if os.path.exists('/home1/main/Desktop/locs/' + '{}_{}.png'
        .format(class_name, im_index)):
        return

    import matplotlib.pyplot as plt
    im = cv2.imread(imdb.image_path_at(im_ind))
    im = im[:, :, (2, 1, 0)]

    fig, (ax1, ax2) = plt.subplots(1, 2)

    # Draw original image
    ax1.imshow(im)
    ax1.set_xticklabels([])
    ax1.set_yticklabels([])
    ax1.set_title('{:s}'.format(im_index))

    # Draw ground-truth bbox
    for i in xrange(gt_boxes.shape[0]):
        ax2.imshow(im)
        bbox = np.array(gt_boxes[i], dtype=np.float32)
        plt.gca().add_patch(
            plt.Rectangle((bbox[0], bbox[1]),
                          bbox[2] - bbox[0],
                          bbox[3] - bbox[1], fill=False,
                          edgecolor='g', linewidth=3)
            )

    # Draw heatmap + localisation point
    sum_heatmap, score_heatmap = get_heatmap([im.shape[0], im.shape[1]], dets)
    draw_heatmap(ax2, sum_heatmap)
    locs = get_locs(sum_heatmap, score_heatmap)
    draw_localisations(locs)
    ax2.set_title('{:s}: {:.3f}'.format(class_name, im_score))
    plt.subplots_adjust(wspace=0, hspace=0)
    # plt.show()
    plt.savefig('/home1/main/Desktop/locs/' + '{}_{}.png'.format(class_name, im_index))


def vis_classification(im, class_name, score, thresh=0.9):
    import matplotlib.pyplot as plt
    import matplotlib.pyplot as plt
    import matplotlib.pyplot as plt
    if score <= thresh:
        return
    im = im[:, :, (2, 1, 0)]
    plt.imshow(im)
    plt.title('{:s}: {:.3f}'.format(class_name, score))
    plt.show()


def vis_detections(imdb, im_ind, class_ind, dets, thresh=0.8):
    """Visual debugging of detections."""

    inds = np.where(dets[:, -1] >= thresh)[0]
    if len(inds) == 0:
        return

    import matplotlib.pyplot as plt

    gt_boxes = imdb.gt_roidb()[im_ind]['boxes'] \
        [np.where(imdb.gt_roidb()[im_ind]['gt_classes'] == class_ind)]
    im_index = imdb.image_index[im_ind]
    class_name = imdb.classes[class_ind]
    im = cv2.imread(imdb.image_path_at(im_ind))
    im = im[:, :, (2, 1, 0)]

    if os.path.exists('/home1/main/Desktop/dets/' + '{}_{}.png'
        .format(class_name, im_index)):
        return

    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect='equal')

    # Draw gt bbox
    for i in xrange(gt_boxes.shape[0]):
        bbox = np.array(gt_boxes[i], dtype=np.float32)
        plt.gca().add_patch(
            plt.Rectangle((bbox[0], bbox[1]),
                          bbox[2] - bbox[0],
                          bbox[3] - bbox[1], fill=False,
                          edgecolor='g', linewidth=3)
            )

    for i in xrange(np.minimum(10, dets.shape[0])):
        bbox = dets[i, :4]
        score = dets[i, -1]
        if score > thresh:
            plt.gca().add_patch(
                plt.Rectangle((bbox[0], bbox[1]),
                              bbox[2] - bbox[0],
                              bbox[3] - bbox[1], fill=False,
                              edgecolor='r', linewidth=3)
                )
            ax.text(bbox[0] + 2, bbox[1] + 9,
                    '{:s} {:.3f}'.format(class_name, score),
                    bbox=dict(facecolor='blue', alpha=0.5),
                    fontsize=14, color='white')

            plt.axis('off')
            plt.tight_layout()
            # plt.show()
            plt.savefig('/home1/main/Desktop/dets/' + '{}_{}.png'
                .format(class_name, im_index))


def apply_nms(all_boxes, thresh):
    """Apply non-maximum suppression to all predicted boxes output by the
    test_net method.
    """
    num_classes = len(all_boxes)
    num_images = len(all_boxes[0])
    nms_boxes = [[[] for _ in xrange(num_images)]
                 for _ in xrange(num_classes)]
    for cls_ind in xrange(num_classes):
        for im_ind in xrange(num_images):
            dets = all_boxes[cls_ind][im_ind]
            if dets.shape[0] == 0:
                continue
            keep = nms(dets, thresh)
            if len(keep) == 0:
                continue
            nms_boxes[cls_ind][im_ind] = dets[keep, :].copy()
    return nms_boxes


def apply_heatmap(im_size, all_boxes):
    num_classes = len(all_boxes)
    num_images = len(all_boxes[0])

    for cls_ind in xrange(num_classes):
        print 'Apply heatmap for class ', cls_ind
        for im_ind in xrange(num_images):
            dets = all_boxes[cls_ind][im_ind]
            if len(dets) == 0:
                continue

            sum_heatmap, score_heatmap = get_heatmap(im_size[im_ind], dets)
            new_dets = get_heatmap_dets(sum_heatmap, score_heatmap, dets)
            if len(new_dets) > 0:
                all_boxes[cls_ind][im_ind] = np.vstack((all_boxes[cls_ind][im_ind], new_dets))


def get_all_locs(im_sizes, all_boxes):
    """
    From all_boxes, calculate highest response point (in term of heatmap)
    for each object class in each image
    """
    num_classes = len(all_boxes)
    num_images = len(all_boxes[0])

    all_locs = [[[] for im_ind in xrange(num_images)]
                for cls_ind in xrange(num_classes)]

    for cls_ind in xrange(num_classes):
        for im_ind in xrange(num_images):
            dets = all_boxes[cls_ind][im_ind]
            sum_heatmap, score_heatmap = get_heatmap(im_sizes[im_ind], dets)
            all_locs[cls_ind][im_ind] = get_locs(sum_heatmap, score_heatmap)

    return all_locs


def test_net(net, imdb):
    """Test a Fast R-CNN network on an image database."""
    num_images = len(imdb.image_index)
    # heuristic: keep an average of 40 detections per class per images prior
    # to NMS
    max_per_set = 40 * num_images
    # heuristic: keep at most 100 detection per class per image prior to NMS
    max_per_image = 100
    # detection thresold for each class (this is adaptively set based on the
    # max_per_set constraint)
    thresh = 0.0 * np.ones(imdb.num_classes)
    # top_scores will hold one minheap of scores per class (used to enforce
    # the max_per_set constraint)
    top_scores = [[] for _ in xrange(imdb.num_classes)]
    # all detections are collected into:
    #    all_boxes[cls][image] = N x 5 array of detections in
    #    (x1, y1, x2, y2, score)
    all_boxes = [[[] for _ in xrange(num_images)]
                 for _ in xrange(imdb.num_classes)]

    all_scores = [[[] for _ in xrange(num_images)]
                 for _ in xrange(imdb.num_classes)]

    im_sizes = [[] for i in xrange(num_images)]

    output_dir = get_output_dir(imdb, net)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # timers
    _t = {'im_detect': Timer(), 'misc': Timer(), 'heatmap': Timer(), 'nms': Timer(), 'locs': Timer()}

    roidb = imdb.roidb
    for i in xrange(num_images):
        im = cv2.imread(imdb.image_path_at(i))
        im_sizes[i] = [im.shape[0], im.shape[1]]
        _t['im_detect'].tic()
        im_scores, scores, boxes = im_detect(net, im, roidb[i]['boxes'])
        _t['im_detect'].toc()
        _t['misc'].tic()

        # for j in xrange(0, imdb.num_classes):
        #     if imdb.classes[j] in ['bottle', 'sofa', 'pottedplant']:
        #         vis_classification(im, imdb.classes[j], im_scores[j])

        for j in xrange(0, imdb.num_classes):
            inds = np.where((scores[:, j] > thresh[j]) &
                            (roidb[i]['gt_classes'] == 0))[0]
            cls_scores = scores[inds, j]
            cls_boxes = boxes[inds, j*4:(j+1)*4]
            top_inds = np.argsort(-cls_scores)[:max_per_image]
            cls_scores = cls_scores[top_inds]
            cls_boxes = cls_boxes[top_inds, :]
            # push new scores onto the minheap
            for val in cls_scores:
                heapq.heappush(top_scores[j], val)
            # if we've collected more than the max number of detection,
            # then pop items off the minheap and update the class threshold
            if len(top_scores[j]) > max_per_set:
                while len(top_scores[j]) > max_per_set:
                    heapq.heappop(top_scores[j])
                thresh[j] = top_scores[j][0]

            all_boxes[j][i] = \
                    np.hstack((cls_boxes, cls_scores[:, np.newaxis])) \
                    .astype(np.float32, copy=False)

            all_scores[j][i] = im_scores[j]

            if 0 and im_scores[j] > 0.8 and imdb.classes[j] in ['cow', 'horse', 'sofa', 'bottle', 'bicycle', 'bird']:
                vis_localisation(imdb, i, j, im_scores[j], all_boxes[j][i])

            if 0:
                keep = nms(all_boxes[j][i], 0.3)
                vis_detections(imdb, i, j, all_boxes[j][i][keep, :])

        _t['misc'].toc()

        print 'im_detect: {:d}/{:d} {:.3f}s {:.3f}s' \
              .format(i + 1, num_images, _t['im_detect'].average_time,
                      _t['misc'].average_time)

    for j in xrange(0, imdb.num_classes):
        for i in xrange(num_images):
            inds = np.where(all_boxes[j][i][:, -1] > thresh[j])[0]
            all_boxes[j][i] = all_boxes[j][i][inds, :]

    det_file = os.path.join(output_dir, 'detections.pkl')
    with open(det_file, 'wb') as f:
        cPickle.dump(all_boxes, f, cPickle.HIGHEST_PROTOCOL)

    if cfg.TEST.HEATMAP_ENABLED:
        print 'Calculating heatmaps'
        _t['heatmap'].tic()
        apply_heatmap(im_sizes, all_boxes)
        _t['heatmap'].toc()
        print 'im_heatmap time: {:.3f}s'.format(_t['heatmap'].total_time)

    print 'Evalutating classification'
    imdb.evaluate_classification(all_scores, output_dir)

    print 'Evalutating locationsation'
    _t['locs'].tic()
    all_locs = get_all_locs(im_sizes, all_boxes)
    _t['locs'].toc()
    print 'locs time: {:.3f}s'.format(_t['locs'].total_time)
    imdb.evaluate_localisation(all_scores, all_locs, output_dir)

    print 'Applying NMS to all detections'
    _t['nms'].tic()
    nms_dets = apply_nms(all_boxes, cfg.TEST.NMS)
    _t['nms'].toc()
    print 'nms time: {:.3f}s'.format(_t['nms'].total_time)
    print 'Evaluating detections'
    imdb.evaluate_detections(nms_dets, output_dir)
