# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Test a Fast R-CNN network on an imdb (image database)."""

from wfast_rcnn.config import cfg, get_output_dir
from utils.timer import Timer
import numpy as np
import cv2
from utils.blob import im_list_to_blob
import os
import caffe

def _centeredCrop(img, new_height, new_width):
    height =  np.size(img,0)
    width =  np.size(img,1)

    left = np.ceil((width - new_width - 2)/2.)
    top = np.ceil((height - new_height - 2)/2.)
    right = np.floor((width + new_width + 2)/2.)
    bottom = np.floor((height + new_height + 2)/2.)

    if (width - new_width) % 2:
        left += 1
    if (height - new_height) % 2:
        top += 1

    cImg = img[top:bottom, left:right,:]
    return cImg


def _get_image_blob(im):
    """Converts an image into a network input.
    """
    image_size = 227
    im = im.astype(np.float32, copy=True)
    # subtract mean
    im -= cfg.PIXEL_MEANS
    # resize im to [256x256]
    im = cv2.resize(im, (227, 227))
    # # image crop to image_size
    # im = _centeredCrop(im, image_size, image_size)
    blob = np.zeros((1, image_size, image_size, 3),
                    dtype=np.float32)
    blob[0, 0:im.shape[0], 0:im.shape[1], :] = im
    # Move channels (axis 3) to axis 1
    # Axis order will become: (batch elem, channel, height, width)
    channel_swap = (0, 3, 1, 2)
    blob = blob.transpose(channel_swap)
    return blob


def _get_blobs(im):
    blobs = {'data': None}
    blobs['data'] = _get_image_blob(im)
    return blobs


def im_cls(net, im):
    # preprocessing im
    blobs = _get_blobs(im)

    # reshape network inputs
    net.blobs['data'].reshape(*(blobs['data'].shape))
    blobs_out = net.forward(data=blobs['data'].astype(np.float32, copy=False))
    im_scores = net.blobs['im_score'].data
    return im_scores


def test_net_baseline(net, imdb):
    num_images = len(imdb.image_index)
    all_scores = [[[] for _ in xrange(num_images)]
                  for _ in xrange(imdb.num_classes)]

    output_dir = get_output_dir(imdb, net)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # timers
    _t = {'im_detect': Timer(), 'misc': Timer()}

    for i in xrange(num_images):
        im = cv2.imread(imdb.image_path_at(i))
        _t['im_detect'].tic()
        im_scores = im_cls(net, im)
        im_scores = im_scores.reshape((imdb.num_classes))
        _t['im_detect'].toc()

        _t['misc'].tic()
        for j in xrange(0, imdb.num_classes):
            all_scores[j][i] = im_scores[j]
        _t['misc'].toc()

        print 'im_detect: {:d}/{:d} {:.3f}s {:.3f}s' \
              .format(i + 1, num_images, _t['im_detect'].average_time,
                      _t['misc'].average_time)

    print 'Evalutating classification'
    imdb.evaluate_classification(all_scores, output_dir)
