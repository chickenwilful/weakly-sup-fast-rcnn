import caffe

from caffe.proto import caffe_pb2
import google.protobuf as pb2
from train import SolverWrapper


class BaselineSolver(SolverWrapper):
    def __init__(self, solver_prototxt, output_dir, pretrained_model=None):
        self.output_dir = output_dir    
        self.solver = caffe.SGDSolver(solver_prototxt)
        if pretrained_model is not None:
            print 'Loading pretrained model weights from {0}'.format(pretrained_model)
            self.solver.net.copy_from(pretrained_model)
        self.solver_param = caffe_pb2.SolverParameter()
        with open(solver_prototxt, 'rt') as f:
            pb2.text_format.Merge(f.read(), self.solver_param)
        self.epoch_size = 80


def train_net_baseline(solver_prototxt, output_dir, pretrained_model=None, max_iters=40):
        sw = BaselineSolver(solver_prototxt, output_dir, pretrained_model=pretrained_model)
        print 'Solving...'
        sw.train_model(max_iters)
        print 'Done solving.'