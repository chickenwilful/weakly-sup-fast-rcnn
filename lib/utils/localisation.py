import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from heatmap import draw_heatmap


def draw_localisations(locs):
    for loc_id in range(locs.shape[0]):
        loc = locs[loc_id]
        if (loc[0] > 0):
            circle = plt.Circle((loc[0], loc[1]), 5, color='b')
            plt.gcf().gca().add_artist(circle)



def get_locs(sum_heatmap, score_heatmap):
    """
    Get localisation point based on sum_heatmap and score_heatmap
    """
    locs = np.zeros((0, 3))
    if sum_heatmap.max() == 0:
        # No detection found --> no loc found
        return locs

    maxpos = np.where(sum_heatmap == sum_heatmap.max())
    x, y = int(np.mean(maxpos[0])), int(np.mean(maxpos[1]))
    locs = np.vstack((locs, (x, y, 0.0)))  # score later will be classification score
    return locs


def get_cluster_centers(sum_heatmap, score_heatmap, dets):
    def get_score(x, y):
        if x < 0:
            return 0
        else:
            return score_heatmap[x][y]

    locs = np.zeros((0, 3))
    if len(dets) == 0:
        return locs
    else:
        maxpos = np.where(sum_heatmap == sum_heatmap.max())
        X = np.zeros((0, 2))
        for id in range(maxpos[0].shape[0]):
            X = np.vstack((X, [maxpos[0][id], maxpos[1][id]]))
        db = DBSCAN(eps=3, min_samples=1).fit(X)
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        labels = db.labels_

        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        print 'num clusters: ', n_clusters_
        for cluster in xrange(n_clusters_):
            pos = np.where(labels == cluster)
            locx = int(np.mean(X[pos, 0]))
            locy = int(np.mean(X[pos, 1]))
            loc = [locx, locy, get_score(locx, locy)]
            locs = np.vstack((locs, loc))

    return locs


if __name__ == '__main__':
    sum_heatmap = np.zeros((100, 100))
    score_heatmap = np.zeros((100, 100))
    sum_heatmap[5:10, 5:10] = 1.0
    sum_heatmap[50:60, 50:60] = 1.0
    draw_heatmap(plt, sum_heatmap)
    locs = get_cluster_centers(sum_heatmap, score_heatmap, [1])
    draw_localisations(locs)
    plt.show()