import numpy as np
from matplotlib import colors


def draw_heatmap(plt, heatmap, show_axes=False):
    plt.imshow(np.transpose(heatmap), alpha=.6)
    if not show_axes:
        plt.axis('off')
    # plt.colorbar(hm)


def get_heatmap(im_size, dets):
    height, width = im_size[0], im_size[1]
    hm = np.zeros((width, height))
    num = np.full((width, height), 0.000001)

    x1 = dets[:, 0].astype(int)
    y1 = dets[:, 1].astype(int)
    x2 = dets[:, 2].astype(int)
    y2 = dets[:, 3].astype(int)
    scores = dets[:, -1]

    for i in range(len(dets)):
        num[x1[i]:x2[i]+1, y1[i]:y2[i]+1] += 1
        hm[x1[i]:x2[i]+1, y1[i]:y2[i]+1] += scores[i]

    sum_hm = hm / max(0.0001, hm.max())
    score_hm = hm / num
    return sum_hm, score_hm


def get_heatmap_dets(sum_heatmap, score_heatmap, dets, thresh_sum=0.7, thresh_score=0.5):

    new_dets = np.zeros((0, 5), dtype=np.float32)

    goodx = np.where(sum_heatmap > thresh_sum)[0]
    goody = np.where(sum_heatmap > thresh_sum)[1]

    lenx, leny = len(goodx), len(goody)
    if lenx > 0:
        x1 = min(goodx)
        y1 = min(goody)
        x2 = max(goodx)
        y2 = max(goody)
        if x1 + 20 < x2 and y1 + 20 < y2:
            avg_score = np.max(score_heatmap[x1:x2+1, y1:y2+1])
            new_dets = np.vstack((new_dets, np.array([x1, y1, x2, y2, avg_score], dtype=np.float32)))
    return new_dets
