#!/usr/bin/env python

import _init_paths
from wfast_rcnn.config import cfg
from wfast_rcnn.test import im_detect, get_locs
import selective_search_ijcv_with_python as selective_search
from utils.cython_nms import nms
from utils.heatmap import get_heatmap, draw_heatmap, get_heatmap_dets
from utils.localisation import get_locs, draw_localisations, get_cluster_centers
from utils.timer import Timer
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse

IMAGE_DIR = '/home1/main/FYP/weakly-sup-fast-rcnn/data/VOCdevkit/VOC2007/JPEGImages'

CLASSES = ('aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat', 'chair',
           'cow', 'diningtable', 'dog', 'horse',
           'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor')

NETS = {'vgg16': ('VGG16',
                  'vgg16_fast_rcnn_iter_40000.caffemodel'),
        'vgg_cnn_m_1024': ('VGG_CNN_M_1024',
                           'vgg_cnn_m_1024_fast_rcnn_iter_150000.caffemodel'),
        'caffenet': ('CaffeNet',
                     'caffenet_wfast_rcnn_iter_150000.caffemodel')}


def vis_detections(im, class_name, dets, thresh=0.5):
    if len(dets) == 0:
        return

    """Draw detected bounding boxes."""
    inds = np.where(dets[:, -1] >= thresh)[0]
    if len(inds) == 0:
        print 'no detection found.'
        return

    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect='equal')
    for i in inds:
        bbox = dets[i, :4]
        score = dets[i, -1]

        ax.add_patch(
            plt.Rectangle((bbox[0], bbox[1]),
                          bbox[2] - bbox[0],
                          bbox[3] - bbox[1], fill=False,
                          edgecolor='red', linewidth=3.5)
            )
        ax.text(bbox[0], bbox[1] - 2,
                '{:s} {:.3f}'.format(class_name, score),
                bbox=dict(facecolor='blue', alpha=0.5),
                fontsize=14, color='white')

    ax.set_title(('{} detections with '
                  'p({} | box) >= {:.1f}').format(class_name, class_name, thresh),
                  fontsize=12)
    plt.axis('off')
    plt.tight_layout()
    plt.draw()


def vis_localisation(im, class_name, dets, thresh=0.0):
    if len(dets) == 0:
        return

    inds = np.where(dets[:, -1] >= thresh)[0]
    if len(inds) == 0:
        print 'no detection found.'
        return

    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect='equal')

    sum_heatmap, score_heatmap = get_heatmap(im.shape[:2], dets)
    locs = get_locs(sum_heatmap, dets)

    draw_heatmap(ax, sum_heatmap)
    draw_localisations(locs)

    ax.set_title(('{} localisations with '
                  'p({} | box) >= {:.1f}').format(class_name, class_name, thresh),
                  fontsize=12)
    plt.tight_layout()
    plt.draw()


def demo_detection(net, im, obj_proposals, classes):

    # Detect all object classes and regress object bounds
    timer = Timer()
    timer.tic()
    im_scores, scores, boxes = im_detect(net, im, obj_proposals)
    timer.toc()
    print ('Detection took {:.3f}s for '
           '{:d} object proposals').format(timer.total_time, boxes.shape[0])

    # Visualize detections for each class
    CONF_THRESH = 0.8
    NMS_THRESH = 0.3
    for cls in classes:
        cls_ind = CLASSES.index(cls)
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        keep = np.where(cls_scores >= CONF_THRESH)[0]
        cls_boxes = cls_boxes[keep, :]
        cls_scores = cls_scores[keep]
        dets = np.hstack((cls_boxes,
                          cls_scores[:, np.newaxis])).astype(np.float32)

        keep = nms(dets, NMS_THRESH)
        dets = dets[keep, :]
        print 'All {} detections with p({} | box) >= {:.1f}'.format(cls, cls,
                                                                    CONF_THRESH)
        vis_detections(im, cls, dets, thresh=CONF_THRESH)


def demo_localisation(net, im, obj_proposals, classes):
    # Detect potential location of object
    timer = Timer()
    timer.tic()
    im_scores, scores, boxes = im_detect(net, im, obj_proposals)
    timer.toc()
    print ('Localisations took {:.3f}s for '
           '{:d} object proposals').format(timer.total_time, boxes.shape[0])

    # Visualize detections for each class
    CONF_THRESH = 0.2
    for cls in classes:
        cls_ind = CLASSES.index(cls)
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        keep = np.where(cls_scores >= CONF_THRESH)[0]
        cls_boxes = cls_boxes[keep, :]
        cls_scores = cls_scores[keep]
        dets = np.hstack((cls_boxes,
                          cls_scores[:, np.newaxis])).astype(np.float32)

        vis_localisation(im, cls, dets, thresh=CONF_THRESH)
        print 'All {} localisations with p({} | box) >= {:.1f}'.format(cls, cls,
                                                                    CONF_THRESH)


def demo(net, image_name, classes):
    # Load the demo image
    im_file = os.path.join(IMAGE_DIR, image_name + '.jpg')
    im = cv2.imread(im_file)

    # calculate proposals
    obj_proposals = selective_search.get_windows([im_file], cmd='selective_search_rcnn')[0]
    obj_proposals = obj_proposals[:, [1, 0, 3, 2]]

    demo_localisation(net, im, obj_proposals, classes)
    demo_detection(net, im, obj_proposals, classes)
    plt.show()


def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Train a W-FRCN network')
    parser.add_argument('--gpu', dest='gpu_id', help='GPU device id to use [0]',
                        default=0, type=int)
    parser.add_argument('--cpu', dest='cpu_mode',
                        help='Use CPU mode (overrides --gpu)',
                        action='store_true')
    parser.add_argument('--net', dest='demo_net', help='Network to use [caffenet]',
                        choices=NETS.keys(), default='caffenet')

    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = parse_args()

    prototxt = os.path.join(cfg.ROOT_DIR, 'models', NETS[args.demo_net][0], 'test.prototxt')
    caffemodel = os.path.join(cfg.ROOT_DIR, 'data', 'wfast_rcnn_models', NETS[args.demo_net][1])

    if not os.path.isfile(caffemodel):
        raise IOError(('{:s} not found.\nDid you run ./data/scripts/'
                       'fetch_fast_rcnn_models.sh?').format(caffemodel))

    if args.cpu_mode:
        caffe.set_mode_cpu()
    else:
        caffe.set_mode_gpu()
        caffe.set_device(args.gpu_id)
    net = caffe.Net(prototxt, caffemodel, caffe.TEST)

    print '\n\nLoaded network {:s}'.format(caffemodel)

    demo(net, '000004', ('car',))
    demo(net, '001551', ('sofa', 'tvmonitor'))
    demo(net, '000053', ('cat',))
    # demo(net, '000011', ('cat',))
    # demo(net, '000059', ('tvmonitor',))
    # # demo(net, '009404', ('tvmonitor',))
    # demo(net, '000015', ('bicycle',))
    # demo(net, '000317', ('car','person',))
    # demo(net, '000331', ('person', 'sofa'))
    # # demo(net, '001218', ('bus', 'car', 'train'))
    #
    # demo(net, '000144', ('bottle',))
    # demo(net, '000151', ('bottle',))
    # demo(net, '000178', ('bottle',))
    # demo(net, '000369', ('bottle',))
    # demo(net, '000414', ('bottle',))
    # demo(net, '000447', ('bottle',))
    # demo(net, '000587', ('bottle',))
    # demo(net, '000606', ('bottle',))
    # demo(net, '000611', ('bottle',))
    #
    # demo(net, '000069', ('boat',))
    # demo(net, '000080', ('boat',))
    # demo(net, '000295', ('boat',))
    # demo(net, '000350', ('boat',))
    # demo(net, '000444', ('boat',))
    # demo(net, '000449', ('boat',))
    #
    # demo(net, '000006', ('chair',))
    # demo(net, '000084', ('chair',))
    # demo(net, '000247', ('chair',))
    # demo(net, '000339', ('chair',))
    # demo(net, '000397', ('chair',))
    # demo(net, '000547', ('chair',))
    # demo(net, '000652', ('chair',))
