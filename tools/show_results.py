import os
import sys
import numpy as np
import argparse
import scipy.io as sio

CLASSES = ('aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat', 'chair',
           'cow', 'diningtable', 'dog', 'horse',
           'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor')


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Get results of a Fast R-CNN network')
    parser.add_argument('--outputdir', dest='output_dir',
                        help='out directory to get results',
                        default=None, type=str)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
   
    args = parser.parse_args()
    return args


def show_predicted_result(output_dir, task):
    predicted = np.zeros(len(CLASSES), dtype=np.float32)
    try:
        for id in xrange(len(CLASSES)):
            cls = CLASSES[id]
            outfile = os.path.join(output_dir, '{0}_{1}_pr.mat'.format(task, cls))
            predicted[id] = sio.loadmat(outfile)['ap'][0][0]
            print '{:.1f}'.format(predicted[id] * 100)
        print 'mAP: {:.2f}'.format(np.mean(predicted) * 100)
    except Exception:
        pass

if __name__ == '__main__':
    args = parse_args()

    output_dir = args.output_dir
    if output_dir is None:
        print 'Please specify output directory'

    print 'Get classification results ...'
    show_predicted_result(output_dir, 'cls')
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print 'Get detection results ...'
    show_predicted_result(output_dir, 'det')

    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print 'Get localisation results ...'
    show_predicted_result(output_dir, 'loc')
