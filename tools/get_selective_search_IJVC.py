import _init_paths
import os
import selective_search_ijcv_with_python as selective_search
import scipy.io as sio
import numpy as np
from utils.timer import Timer


DEVKIT_PATH = '/home1/main/FYP/data/VOCdevkit'
IMG_PATH = os.path.join(DEVKIT_PATH, 'JPEGImages')
OUTPUT_DIR = '/home1/main/FYP/weakly-sup-fast-rcnn/data/selective_search_IJCV_data'


def get_selective_searchIJCV_quality_mode(year, split):
    # get image indexes
    print 'Loading selective search for voc_{0}_{1}'.format(year, split)
    voc_path = os.path.join(DEVKIT_PATH, 'VOC{0}'.format(year))
    index_file = os.path.join(voc_path, 'ImageSets/Main/{1}.txt'.format(year, split))
    with open(index_file, 'r') as f:
        image_indexes = f.read().splitlines()
    print 'number of images: ', len(image_indexes)

    output_dir = os.path.join(OUTPUT_DIR, 'voc_{0}'.format(year))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for im_ind in image_indexes:
        print 'process ', im_ind
        outfile = os.path.join(output_dir, '{0}.mat'.format(im_ind))
        if os.path.exists(outfile):
            print 'Load the image ss already.'
            continue

        img_path = os.path.join(voc_path, 'JPEGImages', im_ind + '.jpg')
        obj_proposals = selective_search.get_windows([img_path], cmd='selective_search_rcnn')[0]

        # swap channels (0, 1, 2, 3) --> (1, 0, 3, 2)
        obj_proposals = obj_proposals[:, [1, 0, 3, 2]]
        print 'num proposal: ', obj_proposals.shape[0]
        sio.savemat(outfile, mdict={'boxes': obj_proposals})


def get_selective_searchIJCV_fast_mode(year, split):
    print 'Loading selective search for voc_{0}_{1}'.format(year, split)
    voc_path = os.path.join(DEVKIT_PATH, 'VOC{0}'.format(year))
    index_file = os.path.join(voc_path, 'ImageSets/Main/{1}.txt'.format(year, split))
    with open(index_file, 'r') as f:
        image_indexes = f.read().splitlines()
    print 'number of images: ', len(image_indexes)

    output_dir = os.path.join(OUTPUT_DIR, 'voc_{0}'.format(year))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    _t = Timer()
    for im_ind in image_indexes:
        _t.tic()
        outfile = os.path.join(output_dir, '{0}_fast.mat'.format(im_ind))
        if os.path.exists(outfile):
            print 'Load the image ss already.'
            continue
        print 'output will be saved into ', outfile
        img_path = os.path.join(voc_path, 'JPEGImages', im_ind + '.jpg')
        obj_proposals = selective_search.get_windows([img_path], cmd='selective_search_rcnn')[0]
        obj_proposals = obj_proposals[:, [1, 0, 3, 2]]        
        print 'num proposal: ', obj_proposals.shape[0]
        sio.savemat(outfile, mdict={'boxes': obj_proposals})
        print 'time: ', _t.toc(average=False)

    print 'generate proposal average time: ', _t.average_time


def get_selective_searchIJCV_faster_mode(year, split):
    print 'Loading selective search IJCV fast mode for voc_{0}_{1}'.format(year, split)
    voc_path = os.path.join(DEVKIT_PATH, 'VOC{0}'.format(year))
    index_file = os.path.join(voc_path, 'ImageSets/Main/{1}.txt'.format(year, split))
    with open(index_file, 'r') as f:
        image_indexes = f.read().splitlines()

    image_paths = [os.path.join(voc_path, 'JPEGImages', im_ind + '.jpg') 
                    for im_ind in image_indexes]

    output_dir = os.path.join(OUTPUT_DIR, 'voc_{0}'.format(year))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    outfile = os.path.join(output_dir, 'voc_{0}_{1}'.format(year, split) + '.mat')

    print 'output will be saved to ', outfile
    selective_search.save_windows(image_paths, outfile, cmd='selective_search_rcnn')

    print 'Read proposals output...'
    all_boxes = sio.loadmat(outfile)['all_boxes']
    sio.savemat(outfile, mdict={'boxes': all_boxes})
    all_boxes = sio.loadmat(outfile)['boxes'].ravel()
    if len(all_boxes) != len(image_paths):
        raise Exception("Something went wrong computing the windows! Number of \
            boxes = {0} but number of images is {1}".format(len(all_boxes), len(image_paths)))
    print 'done.'
    return all_boxes


if __name__ == '__main__':
    for split in ['trainval']:
        get_selective_searchIJCV_fast_mode('2012', split)
    
    # for split in ['train', 'trainval', 'test']:        
    #     get_selective_searchIJCV_quality_mode('2007', split)
