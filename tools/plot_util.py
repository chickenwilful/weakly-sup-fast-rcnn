import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

import matplotlib.pyplot as plt

def plot_error():
    file_name = 'error.txt'
    ylabel = 'Avg MSE'
    xlabel = 'Epoch'
    title = 'Train Mean Square Error Per Epochs'

    with open(file_name, 'r') as f:
        data = [line.split()[0] for line in f.readlines()]
        plt.clf()
        plt.plot(xrange(0, len(data)), data, 'b-')
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.suptitle(title)
        plt.show()
