#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"

LOG="experiments/logs/default_googlenet.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

time ./tools/train_net.py --gpu 0 \
  --solver models/GoogleNet/solver.prototxt \
  --imdb voc_2012_train \
  --iters 150000

time ./tools/test_net.py --gpu 0 \
  --def models/GoogleNet/test.prototxt \
  --net output/default/voc_2012_val/googlenet_wfast_rcnn_iter_150000.caffemodel \
  --imdb voc_2012_val
