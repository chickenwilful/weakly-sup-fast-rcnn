#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"

LOG="experiments/logs/multiscale_caffenet.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

time ./tools/train_net.py --gpu 0 \
  --solver models/CaffeNet/solver.prototxt \
  --weights data/imagenet_models/CaffeNet.v2.caffemodel \
  --imdb voc_2012_train \
  --cfg experiments/cfgs/multiscale.yml \
  --iters 300000

time ./tools/test_net.py --gpu 0 \
  --def models/CaffeNet/test.prototxt \
  --net output/multiscale/voc_2012_train/caffenet_wfast_rcnn_bs_300_lr_0.001000_st_60000_iter_300000.caffemodel \
  --cfg experiments/cfgs/multiscale.yml \
  --imdb voc_2012_val
