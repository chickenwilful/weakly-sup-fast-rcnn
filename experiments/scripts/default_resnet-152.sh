#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"

LOG="experiments/logs/default_resnet152.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

time ./tools/train_net.py --gpu 0 \
  --solver models/ResNet/ResNet-152/solver.prototxt \
  --imdb voc_2007_trainval \
  --cfg experiments/cfgs/batchsize100.yml

time ./tools/test_net.py --gpu 0 \
 --def models/ResNet/ResNet-152/test.prototxt \
 --net output/default/voc_2007_trainval/resnet-152_wfast_rcnn_batchsize_200_iter_80000.caffemodel \
 --imdb voc_2007_test
