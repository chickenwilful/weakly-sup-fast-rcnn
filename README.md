# Weakly supervised learning based on Fast RCNN

* Weakly supervised Fast-RCNN (`W-FRCN`) is a framework for *object detection and localisation* with Fast-RCNN framework.
* The `W-FRCN` model is described in the report [WFRCN Report](https://www.dropbox.com/s/h3nupcvou1c2wrv/FYPFinalReport_NguyenHoangYen.pdf?dl=0)
* The framework only support Python.

### Requirements (Software and hardware)
Same as requirements of [Fast-RCNN](https://github.com/rbgirshick/fast-rcnn)

### Installation
1. Clone the W-FRCN repository

  ```Shell
  # Make sure to clone with --recursive
  git clone --recursive https://gitlab.com/chickenwilful/weakly-sup-fast-rcnn.git
  ```

2. From now on, we assume the `working directory` is the directory that you cloned W-FRCN

   *Ignore notes 1 and 2 if you followed step 1 above.*

   **Note 1:** If you didn't clone  W-RCNN with the `--recursive` flag, then you'll need to manually clone the `caffe-fast-rcnn` submodule:

    ```Shell
    git submodule update --init --recursive
    ```

    # It will clone the submodule caffe-fast-rcnn https://github.com/chickenwilful/caffe-fast-rcnn


    **Note 2:** The `caffe-fast-rcnn` submodule needs to be on the `wfast-rcnn` branch (or equivalent detached state). This will happen automatically *if you follow these instructions*.


3. Follow caffe instruction to build `caffe` and `pycaffe`. You can download my `Makefile.config` for reference. [Makefile.config](http://www.dropbox.com/s/rx45acom1vt4tf0/Makefile.config?dl=0)


4. Download pre-computed W-FRCN detector

    ```Shell
    ./data/scripts/fetch_wfast_rcnn_models.sh
    ```
    See `data/README.md` for more details.

### Demo
*After successfully completing [basic installation](#installation)*, you'll be ready to run the demo.

**Python**

To run the demo

```Shell
./tools/demo.py
```


### Demo on VOC2007

1. Download the training, validation, test data and VOCdevkit

  ```Shell
  wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
  wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
  wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCdevkit_08-Jun-2007.tar
  ```
  
2. Extract all of these tars into one directory named `VOCdevkit`

  ```Shell
  tar xvf VOCtrainval_06-Nov-2007.tar
  tar xvf VOCtest_06-Nov-2007.tar
  tar xvf VOCdevkit_08-Jun-2007.tar
  ```

3. It should have this basic structure

  ```Shell
    $VOCdevkit/                           # development kit
    $VOCdevkit/VOCcode/                   # VOC utility code
    $VOCdevkit/VOC2007                    # image sets, annotations, etc.
    # ... and several other directories ...
    ```

4. Create symlinks for the PASCAL VOC dataset

  ```Shell
    cd ./data
    ln -s $VOCdevkit VOCdevkit2007

    Using symlinks is a good idea because you will likely want to share the same PASCAL dataset installation between multiple projects.

5. [Optional] follow similar steps to get PASCAL VOC 2010 and 2012

6. Follow the next sections to download pre-computed object proposals and pre-trained ImageNet models


### Download pre-computed Selective Search object proposals

Pre-computed selective search boxes can also be downloaded for VOC2007 and VOC2012.

```Shell
cd $FRCN_ROOT
./data/scripts/fetch_selective_search_data.sh
```

This will populate the `$FRCN_ROOT/data` folder with `selective_selective_data`.

### Download pre-trained ImageNet models

Pre-trained ImageNet models can be downloaded for the three networks described in the paper: CaffeNet (model **S**), VGG_CNN_M_1024 (model **M**), and VGG16 (model **L**).

```Shell
cd $FRCN_ROOT
./data/scripts/fetch_imagenet_models.sh
```
These models are all available in the [Caffe Model Zoo](https://github.com/BVLC/caffe/wiki/Model-Zoo), but are provided here for your convenience.

7. Run demo
* Makesure VOCcode/VOCinit.m points to correct dataset. You may need to replace VOC2006 to VOC2012 and set VOC2012 = true if you want to test VOC 2012.

To run more demo with VOC2007 dataset

```Shell
./tools/demo_voc2007.py
# Remember to check the absolute image directory in the file demo_voc2007.py
```

### Usage

**Train** a W-RCNN detector. For example, train a Caffenet network on VOC 2007 trainval:

```Shell
./tools/train_net.py --gpu 0 --solver models/Caffenet/solver.prototxt \
  --weights data/imagenet_models/Caffenet.v2.caffemodel
```

**Test** a W-RCNN detector. For example, test the Caffenet network on VOC 2007 test:

```Shell
./tools/test_net.py --gpu 1 --def models/Caffenet/test.prototxt \
  --net output/default/voc_2007_trainval/caffenet_fast_rcnn_iter_150000.caffemodel
```

Test output is written underneath `./output`.


### Experiment scripts
Scripts to reproduce the experiments in the paper (*up to stochastic variation*) are provided in `$FRCN_ROOT/experiments/scripts`. Log files for experiments are located in `experiments/logs`.

**Note:** the RNG seed for Caffe is fixed, unless `train_net.py` is called with the `--rand` flag.